package backernd;

public class SearchResultItemDTO {

    @MySqlFiledName(name = "Title")
    private String title;

    @MySqlFiledName(name = "Link")
    private String link;

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(final String link) {
        this.link = link;
    }
}
