package frontend.pageobject;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class GoogleSearchMainPage {

    @FindBy(xpath = "//input[@name='q']")
    private WebElement searchInput;

    private WebDriver driver;

    public GoogleSearchMainPage(final WebDriver driver) {
        this.driver = driver;
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        PageFactory.initElements(driver, this);
    }

    public GoogleSearchResultPage searchFor(final String text) {
        searchInput.sendKeys(text, Keys.ENTER);
        return new GoogleSearchResultPage(driver);
    }
}
