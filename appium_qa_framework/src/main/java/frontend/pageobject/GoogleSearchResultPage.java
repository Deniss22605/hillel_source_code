package frontend.pageobject;

import backernd.SearchResultItemDTO;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;

public class GoogleSearchResultPage {

    @FindBys({
            @FindBy(xpath = "//a[@class='C8nzq BmP5tf']")
    })
    private List<WebElement> searchResults;

    private WebDriver webDriver;

    public GoogleSearchResultPage(final WebDriver driver) {
        this.webDriver = driver;
        PageFactory.initElements(driver, this);
    }

    public List<SearchResultItemDTO> getSearchResults() {
        final List<SearchResultItemDTO> searchResultsDTO = new ArrayList<SearchResultItemDTO>();
        for (final WebElement searchItem : searchResults) {

            final String title = searchItem.findElement(By.xpath("(.//div[@role='heading'])[1]")).getText();
            final String link = searchItem.findElement(By.xpath("(.//span)[1]")).getText();

            final SearchResultItemDTO resultItem = new SearchResultItemDTO();
            resultItem.setTitle(title);
            resultItem.setLink(link);

            searchResultsDTO.add(resultItem);
        }
        return searchResultsDTO;
    }
}
