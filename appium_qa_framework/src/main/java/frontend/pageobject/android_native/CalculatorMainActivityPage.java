package frontend.pageobject.android_native;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.AppiumFluentWait;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import utils.AppiumWaitUtils;

import java.util.concurrent.TimeUnit;

public class CalculatorMainActivityPage {

    @AndroidFindBy(id = "com.google.android.calculator:id/formula")
    private MobileElement input;

    @AndroidFindBy(id = "com.google.android.calculator:id/result")
    private MobileElement result;

    @AndroidFindBy(id = "com.google.android.calculator:id/digit_9")
    private MobileElement operandOne;

    @AndroidFindBy(id = "com.google.android.calculator:id/digit_6")
    private MobileElement operandTwo;

    @AndroidFindBy(id = "com.google.android.calculator:id/op_add")
    private MobileElement plusButton;

    @AndroidFindBy(id = "com.google.android.calculator:id/eq")
    private MobileElement equalsButton;

    @AndroidFindBy(id = "com.google.android.calculator:id/pad_pager")
    private MobileElement padPager;

    @AndroidFindBy(id = "com.google.android.calculator:id/pad_advanced")
    private MobileElement numericPanel;

    private AppiumDriver appiumDriver;

    public CalculatorMainActivityPage(final AppiumDriver appiumDriver) throws InterruptedException {
        appiumDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        this.appiumDriver = appiumDriver;
        Thread.sleep(2000);
        PageFactory.initElements(new AppiumFieldDecorator(appiumDriver), this);
    }

    public void typeOperandOne() {
        operandOne.click();
    }

    public void typeOperandTwo() {
        operandTwo.click();
    }

    public void clickPlusButton() {
        plusButton.click();
    }

    public void clickEqualsButton() {
        equalsButton.click();
    }

    public void sendExpression(final String expression) {
        input.sendKeys(expression);
    }

    public Integer getOperationResult() {
        final String operResult = result.getText();
        return Integer.parseInt(operResult);
    }
}
