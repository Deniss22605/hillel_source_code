package utils;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.AppiumFluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class AppiumWaitUtils {

    private AppiumWaitUtils() {

    }

    public static Wait getWait(final AppiumDriver appiumDriver) {
        return new AppiumFluentWait(appiumDriver)
                .withTimeout(20, TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class)
                .ignoring(TimeoutException.class);
    }
}
