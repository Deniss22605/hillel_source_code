package utils;

import backernd.SearchResultItemDTO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MSQLHelper {

    private static final String CONNECTION_STRING = "jdbc:mysql://db4free.net:3306/hillel_db";
    private static final String USER_NAME = "hillel_user";
    private static final String USER_PASSWORD = "Hillel@2019";

    private static Connection connection;
    private static Statement statement;
    private static ResultSet resultSet;

    private MSQLHelper() {
    }

    public static void connectToDb() throws SQLException {
        try {
            connection = DriverManager.getConnection(CONNECTION_STRING, USER_NAME, USER_PASSWORD);
            statement = connection.createStatement();
        } catch (final SQLException e) {
            connection.close();
            statement.close();
            throw new IllegalStateException("Unable to connect to DB!", e);
        }
    }

    public static ResultSet executeQuery(final String sqlQuery) {
        try {
            if (connection.isClosed() || connection == null) {
                connectToDb();
            }
            return statement.executeQuery(sqlQuery);
        } catch (final SQLException e) {
            throw new IllegalStateException("Unable to execute SQL query!", e);
        }
    }

    public static List<SearchResultItemDTO> getResultsAs(final ResultSet resultSet) throws SQLException {
        final List<SearchResultItemDTO> dbResults = new ArrayList<SearchResultItemDTO>();
        while (resultSet.next()) {

            final String title = resultSet.getString("Title");
            final String link = resultSet.getString("Link");

            final SearchResultItemDTO resultItemDTO = new SearchResultItemDTO();

            resultItemDTO.setLink(link);
            resultItemDTO.setTitle(title);

            dbResults.add(resultItemDTO);
        }
        return dbResults;
    }
}
