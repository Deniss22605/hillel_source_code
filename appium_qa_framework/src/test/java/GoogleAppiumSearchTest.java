import backernd.SearchResultItemDTO;
import frontend.pageobject.GoogleSearchMainPage;
import frontend.pageobject.GoogleSearchResultPage;
import frontend.pageobject.android_native.CalculatorMainActivityPage;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.unitils.reflectionassert.ReflectionAssert;
import utils.MSQLHelper;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class GoogleAppiumSearchTest {

    private DesiredCapabilities capabilities = new DesiredCapabilities();
    private AppiumDriver webDriver = null;
    private AppiumServiceBuilder serviceBuilder; //для конфигурации Appium. Установка порта, и хаба
    private AppiumDriverLocalService appiumDriverLocalService; //для запуска и остановки Appium сервиса

    @Before
    public void setUpAppium() throws MalformedURLException {
        final String browser = System.getProperty("browserName");
        if (browser.equalsIgnoreCase("Chrome")) {
            startAppium();
        } else {
            System.out.println("That is not a supported browser!");
        }
    }

    @Test
    public void checkGoogleSearch() throws SQLException {
        final String query = "select * from SearchResults";
        webDriver.get("https://www.google.com/");
        final GoogleSearchMainPage searchMainPage = new GoogleSearchMainPage(webDriver);
        final GoogleSearchResultPage searchResultsPage = searchMainPage.searchFor("Appium");
        final List<SearchResultItemDTO> siteSearchResults = searchResultsPage.getSearchResults();
        MSQLHelper.connectToDb();
        final ResultSet resultSet = MSQLHelper.executeQuery(query);
        List<SearchResultItemDTO> dbSearchResults = MSQLHelper.getResultsAs(resultSet);
        ReflectionAssert.assertReflectionEquals("There are mismatched data found!",
                siteSearchResults, dbSearchResults);
    }

    @Test
    public void checkNativeGoogleCalculator() throws InterruptedException {
        final CalculatorMainActivityPage page = new CalculatorMainActivityPage(webDriver);
        page.typeOperandOne();
        page.clickPlusButton();
        page.typeOperandTwo();
        page.clickEqualsButton();
        final Integer operationResult = page.getOperationResult();
        final Integer expectedOperationResult = 15;
        Assert.assertEquals("There is incorrect operation result calculated!",
                operationResult, expectedOperationResult);
    }

    private void startAppium() {
        capabilities.setCapability("deviceName", "Nexus 6");
        capabilities.setCapability("udid", "e3d3c058");
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("platformVersion", "7.1.1");
        //capabilities.setCapability("browserName", "Chrome");
        capabilities.setCapability("appPackage", "com.google.android.calculator");
        capabilities.setCapability("appActivity", "com.android.calculator2.Calculator");
        serviceBuilder = new AppiumServiceBuilder();
        serviceBuilder.withIPAddress("127.0.0.1");
        serviceBuilder.usingPort(4723);
        serviceBuilder.withCapabilities(capabilities);
        serviceBuilder.withArgument(GeneralServerFlag.SESSION_OVERRIDE);
        serviceBuilder.withArgument(GeneralServerFlag.LOG_LEVEL, "error");
        webDriver = new AndroidDriver(capabilities);
        appiumDriverLocalService = AppiumDriverLocalService.buildService(serviceBuilder);
        appiumDriverLocalService.start();
    }

    private void terminateAppium() {
        appiumDriverLocalService.stop();
    }

    @After
    public void tearDownAppium() {
        //webDriver.close();
        //webDriver.quit();
        terminateAppium();
    }
}
