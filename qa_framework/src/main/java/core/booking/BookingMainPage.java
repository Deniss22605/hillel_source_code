package core.booking;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BookingMainPage {

    @FindBy(xpath = "//input[@id='ss']")
    private WebElement destination;

    @FindBy(xpath = "//div[@data-calendar2-title='Check-in']")
    private WebElement openCalendar;

    @FindBy(xpath = "//label[@id='xp__guests__toggle']")
    private WebElement guests;

    @FindBy(xpath = "//button[@data-sb-id='main']")
    private WebElement searchButton;

    @FindBy(xpath = "//div[@data-bui-ref='calendar-next']")
    private WebElement nextDateButton;

    private static final String CURRENT_MONTH_AND_YEAR = "(//div[@class='bui-calendar__month'])[1]";

    private static final String CURRENT_DAYS = "//div[@class='bui-calendar__wrapper'][1]//td[@data-bui-ref='calendar-date' and contains(@data-date, '%s')]";

    private WebDriver webDriver;
    private WebDriverWait webDriverWait;

    public BookingMainPage(final WebDriver driver) {
        this.webDriver = driver;
        webDriverWait = new WebDriverWait(driver, 30);
        PageFactory.initElements(driver, this);
    }

    public void searchForMonthAndYear(final String monthAndYear) {
        openCalendar.click();
        while (!webDriver.findElement(By.xpath(CURRENT_MONTH_AND_YEAR)).getText().equalsIgnoreCase(monthAndYear)) {
            webDriverWait.until(ExpectedConditions.elementToBeClickable(nextDateButton)).click();
        }
    }

    public void selectDay(final String expectedDay) {
        final String fullDayLocator = String.format(CURRENT_DAYS, expectedDay);
        final WebElement requiredDay = webDriver.findElement(By.xpath(fullDayLocator));
        requiredDay.click();
    }
}
