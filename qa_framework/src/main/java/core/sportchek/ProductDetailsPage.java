package core.sportchek;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class ProductDetailsPage {

    @FindBy(xpath = "//h1[@class='global-page-header__title']")
    private WebElement productTitle;

    @FindBy(xpath = "//div[@class='number-spin-btn number-spin-btn--up']")
    private WebElement addMoreQtys;

    @FindBy(xpath = "//button[contains(@class, 'add-cart product-detail__button')]")
    private WebElement addToCartButton;

    @FindBy(xpath = "//a[contains(@class, 'header-cart__trigger')]")
    private WebElement miniCartItemsCount;

    @FindBy(xpath = "(//section[@class='cart-item']//a)[2]")
    private WebElement miniCartPopupTitle;

    @FindBy(xpath = "//dd[@class='cart-item__detail__description']")
    private WebElement miniCartPopupQty;

    @FindBy(xpath = "//div[@class='spinner']//img[contains(@alt,'Loading...')]")
    private WebElement loadingSpinerIcon;

    @FindBy(xpath = "//input[@id='qty-selector']")
    private WebElement selectedQtyInput;

    @FindBys({
            @FindBy(xpath = "//a[@data-control-type='size']")
    })
    private List<WebElement> productSizesList;

    private WebDriver webDriver;

    private Actions actions;

    private WebDriverWait webDriverWait;

    private Integer productQtyBeforeAddToCart;

    public Integer getProductQtyBeforeAddToCart() {
        return productQtyBeforeAddToCart;
    }

    public ProductDetailsPage(final WebDriver driver) {
        this.webDriver = driver;
        webDriverWait = new WebDriverWait(webDriver, 30);
        actions = new Actions(driver);
        webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        waitForJavaScriptToBeCompleted();
        PageFactory.initElements(driver, this);
    }

    public void selectFirstAvailableProductSize() {
        final int firstSizeListIndex = 1;
        final WebElement firstAvailableProductSize = productSizesList.get(firstSizeListIndex);
        webDriverWait.until(ExpectedConditions.elementToBeClickable(firstAvailableProductSize));
        firstAvailableProductSize.click();
    }

    public void selectProductQty(final String qty) {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(addMoreQtys)).click();
        productQtyBeforeAddToCart = Integer.parseInt(selectedQtyInput.getAttribute("value"));
    }

    public void clickAddToCartButton() {
        addToCartButton.click();
    }

    public String getMiniCartPopupTitle() {
        actions.moveToElement(miniCartItemsCount).perform();
        return miniCartPopupTitle.getText();
    }

    public Integer getMiniCartPopupQty() {
        actions.moveToElement(miniCartItemsCount).perform();
        final String productQty = miniCartPopupQty.getText();
        return Integer.parseInt(productQty);
    }

    public String getProductTitle() {
        return productTitle.getText();
    }

    private void waitForJavaScriptToBeCompleted() {
        webDriverWait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(final WebDriver webDriver) {
                return ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete");
            }
        });

        webDriverWait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(final WebDriver webDriver) {
                return (Boolean) ((JavascriptExecutor) webDriver).executeScript("return jQuery.active == 0");
            }
        });
    }
}
