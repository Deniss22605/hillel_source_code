package booking;

import common.BaseTest;
import core.booking.BookingMainPage;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

public class BookingTestSuite extends BaseTest {

    @Test
    public void checkBookingSearch() {
        final WebDriver driver = getDriver();
        driver.get("https://www.booking.com/index.en-gb.html");
        final BookingMainPage bookingMainPage = new BookingMainPage(driver);
        bookingMainPage.searchForMonthAndYear("February 2019");
        bookingMainPage.selectDay("11");
    }
}
