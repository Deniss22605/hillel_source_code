package google;

import common.BaseTest;
import core.google.GoogleMainPage;
import core.google.SearchResultPage;
import org.junit.*;
import org.openqa.selenium.WebDriver;

public class GoogleTestSuite extends BaseTest {

    @Test
    public void checkGoogleMainPageIsOpened() {
        final WebDriver webDriver = getDriver();
        final String expectedTitle = "Google";
        webDriver.get("https://google.com");
        final String actualTitle = webDriver.getTitle();
        Assert.assertEquals("There is incorrect page title!",
                expectedTitle, actualTitle);
    }

    @Test
    public void checkGoogleSearchFunctionality() {
        final WebDriver webDriver = getDriver();
        final String expectedFirstLinkText = "Компьютерная школа Hillel: курсы IT технологий";

        //1. Открываем главную страницу Google
        webDriver.get("https://google.com");

        //2. Создаем объект главной страницы Google
        final GoogleMainPage page = new GoogleMainPage(webDriver);

        //3. Вводим искомое слово в поисковик
        final SearchResultPage searchResultPage = page.typeSearchText("Hillel");

        //4. Возращаем новую страницу с результатами поиска
        // page.clickSearchButton();

        //5. Получаем текст первой ссылки со списка результатов
        final String actualFirstLinkText = searchResultPage.getFirstSearchResultLinkText();

        //6. Сравниваем експектед текст с ожидаемым текстом
        Assert.assertEquals("There is incorrect first link text displayed!",
                expectedFirstLinkText, actualFirstLinkText);

    }
}
