package sportchek;

import common.BaseTest;
import core.sportchek.ProductDetailsPage;
import core.sportchek.SportCheckMainPage;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import java.util.List;

public class SportChekTestSuite extends BaseTest {

    @Test
    public void checkFilterItems() {
        final WebDriver webDriver = getDriver();
        final String[] expectedFilterItems = {
                "Deals & Features",
                "Men",
                "Women",
                "Kids",
                "Shoes & Footwear",
                "Gear",
                "Electronics",
                "Jerseys & Fan Wear",
                "Sneaker Launches",
                "Shop by Brand",
                "Chek advice"
        };
        webDriver.get("https://www.sportchek.ca/");
        final SportCheckMainPage sportCheckMainPage = new SportCheckMainPage(webDriver);
        sportCheckMainPage.expandFilterBox();
        final List<String> actualFilterItems = sportCheckMainPage.getFilterItemsTextList();
        Assert.assertArrayEquals("There are incorrect items displayed!",
                expectedFilterItems, actualFilterItems.toArray());
    }

    @Test
    public void checkMiniCartProductInfo() {
        final WebDriver driver = getDriver();
        driver.get("https://www.sportchek.ca/");
        final SportCheckMainPage sportCheckMainPage = new SportCheckMainPage(driver);
        final ProductDetailsPage productDetailsPage = sportCheckMainPage.selectFirstElementFromThePickedJustForYouSection();
        productDetailsPage.selectFirstAvailableProductSize();
        productDetailsPage.selectProductQty("2");
        productDetailsPage.clickAddToCartButton();

        final String productTitleBeforeAddToCart = productDetailsPage.getProductTitle();
        final String productTitleAfterAddToCart = productDetailsPage.getMiniCartPopupTitle();

        final Integer productQtyBeforeAddToCart = productDetailsPage.getProductQtyBeforeAddToCart();
        final Integer actualProductQty = productDetailsPage.getMiniCartPopupQty();

        Assert.assertEquals("There is incorrect product title displayed!",
                productTitleBeforeAddToCart, productTitleAfterAddToCart);

        Assert.assertEquals("There is incorrect product qty displayed!",
                productQtyBeforeAddToCart, actualProductQty);
    }
}
